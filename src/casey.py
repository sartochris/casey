'''
Casey - Ensure correct case of your VBA code

Created on 25.03.2016

@author: Christoph Jüngling <christoph@juengling-edv.de>
'''

from configparser import ConfigParser
from os.path import join as pjoin, realpath, isfile
import os
import sys

from argparse import ArgumentParser

from ActionFunctions import patch_all_files
from AnalyzeFunctions import collect_declarations, get_default_file_patterns, \
    get_file_list
import public


def main(argv):
    '''
    Main program

    :param argv: Command line arguments
    :returns: Command line status (Windows: "errorlevel")
    :rtype: Positive integer or 0
    '''

    try:
        args = parse_command_line(argv)
        public.verbose = args.verbose
        folder, _ = getfolders(args)

        if not folder:
            raise Exception('No folder specified! Please call "{} --help" to get information on usage.'.format(public.__appname__))

        if args.create_example_cfg:
            create_example(folder)
            return 0

        filepatterns = getpatterns(args.cfg, folder)
        files = collect_all_files(folder, filepatterns)
        declarations = collect_declarations(files)

        if args.store_config:
            storagefile = pjoin(folder, 'found-elements.txt')
            with open(storagefile, 'w') as storage:
                storage.write('\n'.join([x for x in sorted(declarations)]))

        patch_all_files(files, declarations, args.output, args.no_action, args.sloppy)

        #=======================================================================
        # if messagefile:
        #     with open(messagefile, 'a') as msgfile:
        #         msgfile.write('# Hook executed successfully')
        #=======================================================================

    except Exception as exc:
        print(exc)
        return 1

    return 0


def parse_command_line(arguments):
    '''
    Parse the command line arguments

    :param arguments: List of command line arguments
    :return: Arguments object
    '''
    parser = ArgumentParser(prog='{name} v{version} (#{hash})'.format(name=public.__appname__ ,
                                                                    version=public.__version__,
                                                                    hash=public.__hash__),
                            description=public.__desc__)

    parser.add_argument('pathinfo', nargs='?', help='Path to a file with path information. Used for TGit hook "Start Commit"')
    parser.add_argument('messagefile', nargs='?', help='Path to a file for the commit message. Used for TGit hook "Start Commit"')
    parser.add_argument('cwd', nargs='?', help='Current work directory. Used for TGit hook "Start Commit"')

    parser.add_argument('-d', '--directory',
                        help='Directory containing the source files. Use a dot to indicate the current work directory.')

    parser.add_argument('--cfg',
                        help='Specify configuration file (expected to reside in the FOLDER)')

    parser.add_argument('--githook', action='store_true',
                        help='Switch to Git hook mode. In this case three arguments PATH MESSAGEFILE CWD will be recognised and used.')

    parser.add_argument('--sloppy', '--lazy', '--quick', action='store_true',
                        help='Perform replacement in a sloppy way, which runs much quicker')

    parser.add_argument('--create-example-cfg', action='store_true',
                        help='Create an example configuration file "casey-example.cfg" in the given folder')

    parser.add_argument('--store-config', action='store_true',
                        help='Store found elements into configuration file')

    parser.add_argument('-n', '--no-action', '--dry-run',
                        action='store_true',
                        help='Don\'t modify any file, just print what you would do')

    parser.add_argument('-o', '--output',
                        help='Specify output folder (if not set,'
                        ' original files will be overwritten)')

    parser.add_argument('-v', '--verbose',
                        action='count', default=0,
                        help='Increase verbosity level')

    parser.add_argument('--log-level', dest='loglevel', default='WARNING',
                        help='Specify log level for console messages.'
                        ' LEVEL may be one of DEBUG, INFO, WARN, ERROR, CRITICAL (default: WARN).')

    args = parser.parse_args(arguments)
    return args


def collect_all_files(basefolder, filepatterns):
    '''
    Get all files according to the patterns.

    :param folder: Start folder for the search
    :type folder: String
    :param file_patterns: File patterns
    :type file_patterns: List
    :return: Files to be patched
    :rtype: List
    '''
    file_list = {}

    for pattern in filepatterns:
        file_list.update(get_file_list(pjoin(basefolder, pattern['pattern']), pattern['startat']))

    msg = '{} files to be scanned'.format(len(file_list))

    if public.verbose >= 2:
        print(msg)

    return file_list


def read_file_patterns_from_file(configfile):
    '''
    Read file patterns from INI file.

    :param cfg:
    :return: Filenames with path
    :rtype: List
    '''

    if not isfile(configfile):
        raise FileNotFoundError('Configuration file not found: {}'.format(configfile))

    result = []
    config = ConfigParser()
    config.read(configfile)
    for key in config['Files']:
        result.append({'pattern': key, 'startat': config['Files'][key]})

    if public.verbose >= 2:
        print('{} patterns read from ini file'.format(len(result)))

    return result


def create_example(folder):
    '''
    Create example .cfg file in given folder

    :param str folder: Folder to store example file
    '''

    filename = pjoin(folder, 'casey-example.cfg')
    with open(filename, 'w') as cfgfile:
        cfgfile.write('''[Files]
; Use settings this way:
;    file name pattern = start string for replacements
;
; If replacement shall start at the beginning of the file, use:
;    file name pattern =

M_*.def=
F_*.def=^CodeBehindForm$
R_*.def=^CodeBehindForm$
''')


def getfolders(args):
    '''
    Retrieve folders from arguments

    :param ArgumentParser args: Already parsed command line arguments
    :returns: folder, messagefile
    '''

    folder = None
    messagefile = None

    if args.githook:
        folder = args.cwd
        messagefile = args.messagefile
    else:
        if args.directory:
            folder = realpath(args.directory)

    return folder, messagefile


def getpatterns(cfg, folder):
    '''
    Get file patterns depending on the arguments

    :param str cfg: Name of the config file
    :param str folder: Working folder
    '''

    if cfg:
        filepatterns = read_file_patterns_from_file(pjoin(os.getcwd(), folder, cfg))
    else:
        filepatterns = get_default_file_patterns('access')
    return filepatterns


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))

