'''
Created on 26.03.2016

@author: chris
'''
from pprint import pformat
import codecs
import re

from tqdm import tqdm

from AnalyzeFunctions import get_codec
import public


def patch_all_files(files, declarations, output_folder, no_action, sloppy):
    '''
    Patch all files with the correct case

    :param list files: Files to be patched
    :param list declarations: Any declared element (variable, function, ...)
    :param output_folder:
    :type output_folder:
    :param bool no_action: If True, no real action (i.e. file change) is taken, just some statistics are printed
    :param bool sloppy: If True, use the sloppy way to replace, which runs quicker
    '''

    if public.verbose >= 1:
        if sloppy:
            print('Sloppy mode')
        else:
            print('Replace carefully')

    for filename in tqdm(files, desc='Patch files'):
        with codecs.open(filename, 'r', encoding=get_codec()) as file:
            content = file.read()
            if sloppy:
                new_content = replace_content_sloppy(content, declarations)
            else:
                new_content = replace_content_carefully(content, declarations)

        if content != new_content:
            if not no_action:
                if output_folder:
                    raise NotImplementedError()

                with codecs.open(filename, 'w', encoding=get_codec()) as file:
                    file.write(new_content)


def replace_content_sloppy(content, declarations):
    '''
    Simply replace declarations in line

    :param str content: Original file content
    :param list declarations: Declarations found before
    :returns: Content, partly replaced
    '''

    new_content = content
    for decl in declarations:
        new_content = re.sub('\\b' + decl + '\\b', decl, new_content, flags=re.IGNORECASE)  # @UndefinedVariable

    return new_content


def replace_content_carefully(content, declarations):
    '''
    Change content according to the declarations

    :param content: Old content
    :param list declarations: Declarations (from code)
    :return: New content
    :rtype: String
    '''
    new_content = []
    count = 0

    for line in tqdm(content.split('\n'), desc='Line'):
        parts = split_line_into_parts(line)
        newline = replace_parts_of_line(declarations, parts)
        new_content.append(newline)

        if line != newline:
            count += 1

    if public.verbose >= 1:
        print('{} lines have been changed'.format(count))

    return '\n'.join(new_content)


def split_line_into_parts(line):
    '''
    Split the line into parts, regarding on the string and comment delimiters

    :param str line: Current line of code to be checked/changed
    :return: Parts alternating code - no code - code - no code ...
    :rtype: List
    '''

    code = True
    comment = False
    oldcode = True
    oldposition = 0
    line_parts = []

    for position in range(len(line)):
        if line[position] == '"':
            if not comment:
                code = not code
        elif line[position] == "'":
            if code:
                comment = True
                code = False

        if code != oldcode :
            line_parts.append(line[oldposition:position])
            oldposition = position
            oldcode = code

    line_parts.append(line[oldposition:])

    return line_parts


def replace_parts_of_line(declarations, parts):
    '''
    Replace every second part of the line which contain code.

    First part: replace
    Second part: don't replace
    Third part: replace
    etc.

    :param list declarations: Declarations found before
    :param list parts: Parts of the line
    '''

    new_content = []
    shall_replace = True

    for part in parts:
        newpart = part
        if shall_replace and part:
            for decl in declarations:
                newpart = re.sub(
                    '\\b' + decl + '\\b',
                    decl, newpart,
                    flags=re.IGNORECASE)  # @UndefinedVariable

        new_content.append(newpart)
        shall_replace = not shall_replace

    return ''.join(new_content)
