# pylint: disable=line-too-long

'''
Public declarations used project-wide

.. data:: int EXIT_OK = 0

    Exit code for "everything is ok".
    This exit code is set if no error occurred during the tests.

.. data:: int EXIT_ERROR = 1

    Exit code for "something went wrong".
    This exit code is set if there has been any error during the tests detected.

.. data:: bool verbose

    Mirrors the ``-v/--verbose`` command line option for public access

.. data:: bool debug

    Mirrors the ``--debug`` command line option for public access
'''

# Program properties
__appname__ = 'Casey'
__desc__ = 'Walk through your code and change every name according to its declaration'
__version__ = '0.3.2'
__beta__ = True
__hash__ = 'DEADC0DE'

# Command line exit codes
EXIT_OK = 0
EXIT_ERROR = 1

verbose = 0  # pylint: disable=invalid-name
debug = False  # pylint: disable=invalid-name
