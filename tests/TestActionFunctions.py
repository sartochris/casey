'''
Test module for ActionFunctions
'''
import unittest

from ActionFunctions import replace_content_sloppy, replace_content_carefully
from AnalyzeFunctions import get_declaration_patterns, get_declarations_from_content


class TestActionFunctions_Sloppy(unittest.TestCase):
    '''
    Unit tests for module "ActionFunctions" with "sloppy" option
    '''

    def setUp(self):
        self._declaration_patterns = get_declaration_patterns()

    def tearDown(self):
        pass

    def testCommentAfterDeclaration(self):
        original = """
Dim x As Integer ' This is a comment for X
x = 5
X = 6
"""
        expected = """
Dim x As Integer ' This is a comment for x
x = 5
x = 6
"""

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['x'])

        actual = replace_content_sloppy(original, declarations)
        self.assertEqual(actual, expected)

    def testCommentAfterString(self):
        original = 'Const text = "We don\'t want to see Text" \' This is a constant for TEXT'
        expected = 'Const text = "We don\'t want to see text" \' This is a constant for text'

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['text'])

        actual = replace_content_sloppy(original, declarations)
        self.assertEqual(actual, expected)

    def testDoubleQuotes(self):
        original = 'Const TEXT = "This is a ""small"" test text.'
        expected = 'Const TEXT = "This is a ""small"" test TEXT.'

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['TEXT'])

        actual = replace_content_sloppy(original, declarations)
        self.assertEqual(actual, expected)

    def testreplace_parts_of_line(self):
        pass


class TestActionFunctions_Carefully(unittest.TestCase):
    '''
    Unit tests for module "ActionFunctions" with "carefully" option
    '''

    def setUp(self):
        self._declaration_patterns = get_declaration_patterns()

    def tearDown(self):
        pass

    def testCommentAfterDeclaration(self):
        original = """
Dim x As Integer ' This is a comment for X
x = 5
X = 6
"""
        expected = """
Dim x As Integer ' This is a comment for X
x = 5
x = 6
"""

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['x'])

        actual = replace_content_carefully(original, declarations)
        self.assertEqual(actual, expected)

    def testCommentAfterString(self):
        original = 'Const text = "We don\'t want to see Text" \' This is a constant for TEXT'
        expected = 'Const text = "We don\'t want to see Text" \' This is a constant for TEXT'

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['text'])

        actual = replace_content_carefully(original, declarations)
        self.assertEqual(actual, expected)

    def testDoubleQuotes(self):
        original = 'Const TEXT = "This is a ""small"" test text.'
        expected = 'Const TEXT = "This is a ""small"" test text.'

        declarations = get_declarations_from_content(original, self._declaration_patterns)
        self.assertListEqual(declarations, ['TEXT'])

        actual = replace_content_carefully(original, declarations)
        self.assertEqual(actual, expected)

    def testreplace_parts_of_line(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
