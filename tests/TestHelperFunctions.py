'''
Created on 04.11.2019

@author: Christoph.Juengling
'''
from _io import StringIO
from os.path import join as pjoin
from tempfile import TemporaryDirectory
import unittest

from casey import create_example, read_file_patterns_from_file, getpatterns


class TestHelperFuncs(unittest.TestCase):

    def test_create_example(self):
        with TemporaryDirectory() as tempdir:
            create_example(tempdir)

            filename = pjoin(tempdir, 'casey-example.cfg')
            with open(filename) as f:
                content = f.read()

        expected = '''[Files]
; Use settings this way:
;    file name pattern = start string for replacements
;
; If replacement shall start at the beginning of the file, use:
;    file name pattern =

M_*.def=
F_*.def=^CodeBehindForm$
R_*.def=^CodeBehindForm$
'''
        self.assertEqual(content, expected)

    def test_read_file_patterns_from_file(self):
        with TemporaryDirectory() as tempdir:
            create_example(tempdir)
            filename = pjoin(tempdir, 'casey-example.cfg')
            patterns = read_file_patterns_from_file(filename)

        self.assertEqual(len(patterns), 3)
        self.assertEqual(patterns[0]['pattern'], 'm_*.def')
        self.assertEqual(patterns[1]['pattern'], 'f_*.def')
        self.assertEqual(patterns[2]['pattern'], 'r_*.def')
        self.assertEqual(patterns[0]['startat'], '')
        self.assertEqual(patterns[1]['startat'], '^CodeBehindForm$')
        self.assertEqual(patterns[2]['startat'], '^CodeBehindForm$')

    def test_getpatterns_defaults(self):
        patterns = getpatterns('', 'any folder')
        self.assertEqual(len(patterns), 3)
        self.assertEqual(patterns[0]['pattern'], 'm_*.def')
        self.assertEqual(patterns[1]['pattern'], 'f_*.def')
        self.assertEqual(patterns[2]['pattern'], 'r_*.def')
        self.assertEqual(patterns[0]['startat'], '')
        self.assertEqual(patterns[1]['startat'], '^CodeBehindForm$')
        self.assertEqual(patterns[2]['startat'], '^CodeBehindForm$')

    def test_getpatterns_file(self):
        with TemporaryDirectory() as tempdir:
            create_example(tempdir)
            patterns = getpatterns('casey-example.cfg', tempdir)

        self.assertEqual(len(patterns), 3)
        self.assertEqual(patterns[0]['pattern'], 'm_*.def')
        self.assertEqual(patterns[1]['pattern'], 'f_*.def')
        self.assertEqual(patterns[2]['pattern'], 'r_*.def')
        self.assertEqual(patterns[0]['startat'], '')
        self.assertEqual(patterns[1]['startat'], '^CodeBehindForm$')
        self.assertEqual(patterns[2]['startat'], '^CodeBehindForm$')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testCreate_example']
    unittest.main()
