# Requirements for this project
#
# This file specifies the list of requirements
# used for to run and contribute.
#
# Usage:
#    pip install -r requirements.txt
#    pip install -r requirements.txt --proxy localhost:3128
#
ArgumentParser
emit
pprint
pyinstaller
tqdm