Introduction
============

Why did I write this program?
-----------------------------

For a very long time I found myself working with Visual Basic 6 and some VBA programs.
Although those languages are quite nice to use, some *special effects* with the
integrated development environments are a bit weird. One of these effects is the randomly
changed *case* of the types, variables, or functions. This is sometimes really annoying,
at least when I started to use source code control. Although this is irrelevant for the
compiler, the source code control is very accurate with this.

It leads to a huge amount of changes, which aren't intended, and which aren't changes at all.
If you look for a bug or a particular change, this is totally messed up.

So I decided to write autocorrection.py_. While this was indeed helpful, I was unhappy with the
need to specify any single word with its spelling and casing. In the beginning this needs a lot
of justification. A second point was the greed of it: It changed everything it could find, even
comments or strings.

Then I had the idea to gain the information on the correct casing from the program itself. This is
of course not always possible, because Visual Basic doesn't force you to declare variables. So if you
don't, this program may not be helpful for you!


Who needs Casey?
----------------

In my opinion there are just a view people in the world who need Casey:

- me (that's why I wrote it)
- other software developers using *Visual Basic 6* (yes, it's quite old, but still on the working side for some people around)
- other software developers using *Visual Basic for Applications*, which is part of Microsoft Word, Excel, Access and some more
- those of the above, who use a source code control tool to collect their mistakes and ensure they may be found and fixed some day :-) 


And who doesn't?
----------------

Probably *VB.Net* developers don't need Casey, as the current version of Visual Studio handles this
part much better than previous versions. Hopefully.

.. _usage:

Usage
-----

As of v1.0.0, there is just a small command line interface.
This may be changed in the future, so take care of your scripts!

::

    $ casey --help
    usage: casey v0.1.0 [-h] [-n] [-v] folder

    Casey - Ensure correct case of your VBA code

    positional arguments:
      folder                Folder containing the source files

    optional arguments:
      -h, --help            show this help message and exit
      -n, --no-action, --dry-run
                            Don't modify any file, just print what you would do
      -v, --verbose         Increase verbosity level


Folder
......

This is the most important argument, and it is mandatory. It tells Casey in which folder
it has to work. There is no recursive walk-through the file system yet, and at the moment
I don't see any need for it (though it would be easy to implement). Feel free to file a
requirement in Gitlab_ if you think you will need it. 


Help
....

You may know the ``-h`` or ``--help`` command line option from other programs. As itself says,
this command will "show this help message and exit".


No action
.........

The options ``-n``, ``--no-action`` or ``--dry-run`` will only perform the analyzing part of the program
and print some statistical information on the found declarations. There are no changes to the
files made with those options.

If you don't specify it, changes **will** be made!


Verbose
.......

You may increase the verbosity level of the program to some extent. This can be done by specifying
``-v``, ``-vv``, ``-vvv``, etc. Usually this is only necessary for debugging purposes, it won't
probably not be needed during normal operations.


.. _autocorrection.py: https://bitbucket.org/juengling/autocorrection
.. _Gitlab: https://gitlab.com/juengling/casey/issues
