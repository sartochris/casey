.. Casey documentation master file, created by
   sphinx-quickstart on Fri Apr  8 19:05:58 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Casey ensures correct case of your VBA code
===========================================

Casey tries to find any declaration (constants, variables, subs, functions, etc.)
in your VBA code and remembers the correct case-sensitive spelling.

In a second step, casey will walk again through your files and change the case
of any case-insensitively matching word to the case-sensitive spelling from the first step.


Introduction
============

.. toctree::
   
   intro
   installation
   orga
   license
   integration
   development

Code Documentation
==================

.. toctree::
   :maxdepth: 1
   
   main
   analyze
   actions
   unittests

