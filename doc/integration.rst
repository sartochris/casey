Integration Issues
==================

If this program is helpful for you, some day you may want to integrate it
into your favorite source code control. With this chapter I'll try to help
you with that or give at least some assistance doing it yourself.

For sure, I know how to handle `Mercurial <https://www.mercurial-scm.org/>`_,
in short called "Hg".
This will be explained in the next section. Others may follow in a while.

Mercurial
---------

The integration into a local mercurial repository is quite easy. But don't forget
to rearrange this if you cloned it. Hg will never copy any setting from
the base repository into the destination, neither if a clone is performed from
a server, nor from local!

As you may know, Hg manages the repository and its configuration in a subdirectory
called ``.hg``. Here you'll find a file ``hgrc``, which looks somewhat like this
right after the first "clone" and some small enhancements::

   [paths]
   default = ssh://hg@bitbucket.org/youraccount/yourprojectname
   
   [extensions]
   mq = 
   rebase = 
   
   [mq]
   secret = True

You surely know the structure of this file, it's usually called ".ini" file.
In fact it doesn't matter which settings there already are.

Now we have to specify a "precommit hook", which is run before starting a commit.
Hooks are called “triggers” in some revision control systems, but the two names
refer to the same idea.

Just add the following code to the hgrc file at the end::

   [hooks]
   precommit = casey FOLDER

Please replace the ``FOLDER`` expression by the path to the folder where your VB(A)
source files reside. If the installation folder of Casey is not part of the PATH,
you have to specify this as well, for example::

   [hooks]
   precommit = "C:\Program Files\casey\casey" FOLDER

   
Git
---

Because I'm using Git instead of Hg since a couple years, this chapter will surely be
written when Casey runs properly in my current projects.
