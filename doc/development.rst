Development
===========

Here we collect some resources for developers.


To support Windows XP
---------------------

The latest Python version which supports Windows XP is Python 3.4.
This is necessary for projects using Visual Basic 6, because this
is supposed not to run under newer operating systems than Windows
XP under all circumstances.


Pyinstaller für Python 3.4
^^^^^^^^^^^^^^^^^^^^^^^^^^

https://github.com/pyinstaller/pyinstaller/releases/tag/3.0

Herunterladen, dann „pip install PyInstaller-3.0.zip“
